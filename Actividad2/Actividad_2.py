import numpy as np
import cv2
from matplotlib import pyplot as plt
from tkinter import *
from tkinter.filedialog import askopenfilename
ITERACIONES = 1
UMBRAL = 0
GAMMA = 1.5
#PARTE 3 A}


def tercerMetodo():  # shades-of-gray
    sumaR = 0
    sumaB = 0
    sumaG = 0

    p = gama_valor.get()
    nombre_img = askopenfilename()
    esta_hueva = cv2.imread(nombre_img)
    imagen = Gamma(esta_hueva)
    #nombre_img = desdistorcion(nombre_img2)
    valorMaxB = np.amax(imagen)
    valorMaxG = np.amax(imagen)
    valorMaxR = np.amax(imagen)

    imagen[:, :, 0] = (imagen[:, :, 0]/valorMaxB) * 255
    imagen[:, :, 1] = (imagen[:, :, 1]/valorMaxG) * 255
    imagen[:, :, 2] = (imagen[:, :, 2]/valorMaxR) * 255
    cv2.imwrite("original.jpg",imagen)
    cv_img2 = imagen.copy()
    cv_img = imagen.copy()

    fil = cv_img.shape[0]
    col = cv_img.shape[1]

    for x in range(fil):
        for y in range(col):
            b = cv_img.item(x, y, 0)
            g = cv_img.item(x, y, 1)
            r = cv_img.item(x, y, 2)

            b2 = pow(b, p)
            sumaB = sumaB + float(b2)

            g2 = pow(g, p)
            sumaG = sumaG + float(g2)

            r2 = pow(r, p)
            sumaR = sumaR + float(r2)

    print(sumaB)
    print(sumaG)
    print(sumaR)

    raizB = pow(sumaB, float(1/p))
    raizG = pow(sumaG, float(1/p))
    raizR = pow(sumaR, float(1/p))

    print(raizB)
    print(raizG)
    print(raizR)

    kb = raizG/raizB
    kg = raizG/raizG
    kr = raizG/raizR

    print('kb', kb)
    print('kg', kg)
    print('kr', kr)

    for x in range(fil):
        for y in range(col):
            tempB = int(kb * cv_img.item(x, y, 0))
            cv_img2.itemset((x, y, 0), tempB)
            tempG = int(kg * cv_img.item(x, y, 1))
            cv_img2.itemset((x, y, 1), tempG)
            tempR = int(kr * cv_img.item(x, y, 2))
            cv_img2.itemset((x, y, 2), tempR)
    #imgenNormalizada = normalizar(cv_img2)
    cv2.imwrite("codificado.jpg",cv_img2)
    cv2.imshow("original Shades-of-gray", cv2.imread("original.jpg"))
    cv2.imshow("Shades-of-gray", cv2.imread("codificado.jpg"))
    


def primerMetodo():  # Gray-world
    sumaR = 0
    sumaG = 0
    sumaB = 0
    r2 = 0
    g2 = 0
    b2 = 0
    
    KB = 0
    KG = 0
    KR = 0
    #nombre_img= entrada.get()
    img = askopenfilename()
    esta_hueva = cv2.imread(img)
    
    imagen = Gamma(esta_hueva)
    #nombre_img = desdistorcion(nombre_img2)
    valorMaxB = np.amax(imagen[:, :, 0])
    valorMaxG = np.amax(imagen[:, :, 1])
    valorMaxR = np.amax(imagen[:, :, 2])
    
    imagen[:, :, 0] = (imagen[:, :, 0]/valorMaxB) * 255
    imagen[:, :, 1] = (imagen[:, :, 1]/valorMaxG) * 255
    imagen[:, :, 2] = (imagen[:, :, 2]/valorMaxR) * 255
    cv2.imwrite("original.jpg", imagen)
    #nombre_img = desdistorcion(nombre_img2)  # imagen desdistorcionada
    #cv2.imshow("hola",nombre_img)
    cv_img = imagen

    cv_img2 = cv_img.copy()

    fil = cv_img.shape[0] #y
    col = cv_img.shape[1] #x

    for x in range(col):
        for y in range(fil):
            
            r2 = r2 + cv_img.item(y,x,2)
            g2 = g2 + cv_img.item(y,x,1)
            b2 = b2 + cv_img.item(y,x,0)

    print("r2:"+str(r2)) 
    print("g2:"+str(g2)) 
    print("b2:"+str(b2)) 
    
    KB = 1
    KG = r2/b2
    KR = r2/r2

    print("Kb:"+str(KB))
    print("Kg:"+str(KG))
    for x in range(col):
        for y in range(fil):
            RED = cv_img.item(y,x,2) * KR
            GREEN = cv_img.item(y,x,1) * KG

            cv_img2.itemset((y,x,0), cv_img.item(y,x,0))
            cv_img2.itemset((y,x,1), GREEN)
            cv_img2.itemset((y,x,2), RED)
    cv2.imwrite("codificado.jpg",cv_img2)
    cv2.imshow("original - Gray-world", cv2.imread("original.jpg"))
    cv2.imshow("Gray-world", cv2.imread("codificado.jpg"))
    cv2.waitKey(0)
    cv2.destroyAllWindows()


def Gamma(imagen):  # pixel elevado a la gamma
    imagen_2 = imagen.copy()
    valor_g = float(GAMMA)
    print("valor gama", valor_g)
    imge = imagen**valor_g
    cv2.imwrite("distorcionada.jpg",imge)
    cv2.imshow("Distorcionada", cv2.imread("distorcionada.jpg"))
    image = pow(imge, (1/2.2))
    return image


def segundoMetodo():  # scale-by-mx
    valor_mayor = 0
    nombre_img = askopenfilename()
    esta_hueva = cv2.imread(nombre_img)
    imagen = Gamma(esta_hueva)
    #nombre_img = desdistorcion(nombre_img2)
    valorMaxB = np.amax(imagen[:, :, 0])
    valorMaxG = np.amax(imagen[:, :, 1])
    valorMaxR = np.amax(imagen[:, :, 2])

    imagen[:, :, 0] = (imagen[:, :, 0]/valorMaxB) * 255
    imagen[:, :, 1] = (imagen[:, :, 1]/valorMaxG) * 255
    imagen[:, :, 2] = (imagen[:, :, 2]/valorMaxR) * 255

    cv2.imwrite("original.jpg",imagen)

    cv_img = imagen
    cv_img2 = cv_img.copy()
    #cv2.imshow("hola",cv_img)
    fil = cv_img.shape[0]
    col = cv_img.shape[1]
    #Se debe de colocar BGR
    for x in range(fil):
        for y in range(col):
            if (cv_img[x, y] > valor_mayor).any():
                valor_mayor = cv_img[x, y]

    b = valor_mayor[0]
    g = valor_mayor[1]
    r = valor_mayor[2]

    Kr = 1
    Kb = r/b
    Kg = r/g
    print("Kb:"+str(Kb))
    print("Kg:"+str(Kg))

    for x in range(fil):
        for y in range(col):
            b, g, r = cv_img[x, y]
            r2 = r*Kr
            b2 = b*Kb
            g2 = g*Kg
            cv_img2[x, y] = b2, g2, r2
    cv2.imwrite("codificado.jpg",cv_img2)
    cv2.imshow("Scale-by-mx", cv2.imread("codificado.jpg"))
    cv2.imshow("Original Scale-by-mx", cv2.imread("original.jpg"))

#PARTE 2 A - B
def umbralizado_global():
    print("umbralizado_global")
    img = cv2.imread("page1.jpg", 0)
    # Otsu's thresholding
    ret2,th2 = cv2.threshold(img,UMBRAL,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
    print(cv2.THRESH_OTSU)
    histograma = cv2.calcHist([img], [0], None, [256], [0, 256])
    plt.subplot (1,3,1), plt.imshow (img, 'gray' ), plt.title ( 'Imagen original' )
    plt.subplot (1,3,2), plt.plot(histograma, color='gray'), plt.title ( 'Histograma Umbral 0 ')
    plt.subplot (1,3,3), plt.imshow (th2, 'gray' ), plt.title ( 'Imagen otsu' )
    plt.show()

def umbralizacion_local():
    print("UMBRALIZACION LOCAL")
    gray = cv2.imread("paginas.jpg", cv2.IMREAD_GRAYSCALE)
    # umbral adaptable
    gray = cv2.medianBlur(gray, 5)
    dst2 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)
    plt.subplot(1, 3, 1), plt.imshow(gray, 'gray'), plt.title('Imagen original')
    plt.subplot(1, 3, 2), plt.imshow(dst2, 'gray'), plt.title('Imagen Umbral Adaptativo')

    hist = cv2.calcHist([gray], [0], None, [256], [0, 256])
    #plt.plot(hist, color='gray')
    plt.subplot (1,3,3), plt.plot(hist, color='gray'), plt.title ('Histograma')
    plt.xlabel('intensidad de iluminacion')
    plt.ylabel('cantidad de pixeles')
    plt.show()
    cv2.destroyAllWindows()

#PARTE 2 C 
def substraccion_de_fondo():
    print("SUBSTRACCION DE FONDO")
    kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
    image = cv2.imread("paginas.jpg")
    gray = cv2.imread("paginas.jpg", cv2.IMREAD_GRAYSCALE)
    dilatacion = cv2.dilate(gray, kernel, iterations=1)
    diff_total = cv2.absdiff(dilatacion, gray)
    t, dst = cv2.threshold(diff_total, 100, 255, cv2.THRESH_BINARY)
    cv2.imshow("Letras", dst)
    cv2.imshow("Fondo",dilatacion)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

#PARTE 1 
def Contador_Objetos():
    nameImg = askopenfilename()
    gray = cv2.imread(nameImg,cv2.IMREAD_GRAYSCALE)
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(20,30))
    print(kernel)
    t, dst = cv2.threshold(gray, 200, 250, cv2.THRESH_BINARY)
    inversion = np.invert(dst)
    erosion= cv2.erode(inversion, kernel, iterations=ITERACIONES) 
    dilatacion = cv2.dilate(erosion, kernel, iterations=ITERACIONES)

    #contamos los objetos encontrados
    gauss = cv2.GaussianBlur(dilatacion, (5, 5), 0)
    ##canny = cv2.Canny(gauss, 50, 150)#filtro detecta bordes
    contornos,_ = cv2.findContours(dst,1,1) #cuenta los bordes encontrados
    print("-------------------------------------------------------------------------")
    areas = [cv2.contourArea(c) for c in contornos]
    i = 0
    for c in areas:
        if(c > 600):
            i = i + 1

    print("He encontrado {} rectangulos".format(i-1))

    cv2.imshow("Image_Binario",dst)
    cv2.imshow("Dilatacion",dilatacion)
    #cv2.imshow("Erosion",erosion)
    cv2.waitKey(0)
    cv2.destroyAllWindows()


ventana = Tk()
gama_valor = DoubleVar()
ventana.title("Inteligencia Artificial")  # nombre de la ventana
ventana.geometry("300x200")  # tamaño de la ventana

Button(ventana, text="Conteo de objetos", command=Contador_Objetos).place(x=100, y=0)
Button(ventana, text="Umbralizado Local", command=umbralizacion_local).place(x=100, y=30)
Button(ventana, text="Umbralizado Global", command=umbralizado_global).place(x=100, y=60)
Button(ventana, text="Substraccion", command=substraccion_de_fondo).place(x=100,y=90)
Button(ventana, text="Gray-world", command = primerMetodo).place(x=20, y=120)
Button(ventana, text="Scale-by-mx", command=segundoMetodo).place(x=100, y=120)
Button(ventana, text="Shades-of-gray", command=tercerMetodo).place(x=180, y=120)
Label(ventana, text="Valor de P").place(x=120,y=150)
gama_entry = Entry(ventana, textvariable=gama_valor).place(x=180, y=150)
ventana.mainloop()
