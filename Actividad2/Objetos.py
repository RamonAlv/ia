import cv2
import numpy as np
imgen = 'Cuadrados.jpg'

src = cv2.imread(imgen)

gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)

t, dst = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)

kernel = np.ones((4,4),np.uint8)
kernel2 = cv2.getStructuringElement(cv2.MORPH_RECT, (20,40))

dst = np.invert(dst)

erosion = cv2.erode(dst,kernel2,iterations = 1)
dilation = cv2.dilate(erosion,kernel2,iterations = 1)

cv2.imshow('dilatado', dilation)

# gray = cv2.GaussianBlur(dilation, (7, 7), 3)

# t, dst = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)

contours = cv2.findContours(gray, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
i=0
Objetos = 0
for c in contours:
    perimetro = cv2.arcLength(c ,True)
    approx = cv2.approxPolyDP(c, 0.04 * perimetro, True)
    if len(approx) == 4 and i>0:
        (x, y, w, h) = cv2.boundingRect(approx)
        ar = w / float(h)
        if ar >= 0.95 and ar <= 1.05: 
            cv2.drawContours(src, [c], 0, (0, 255, 0), 2, cv2.LINE_AA)
            Objetos = Objetos+1
        
    i=i+1
Num = "Cuadrados: " + str(Objetos)
cv2.imshow(Num, src)
# cv2.imshow('umbral', dst)

cv2.waitKey(0)