import numpy as np
import cv2
from matplotlib import pyplot as plt
from tkinter import *
from tkinter.filedialog import askopenfilename

nameImg = askopenfilename()
gray = cv2.imread(nameImg,cv2.IMREAD_GRAYSCALE)
kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(20,30))
print(kernel)
t, dst = cv2.threshold(gray, 200, 250, cv2.THRESH_BINARY)
inversion = np.invert(dst)
erosion= cv2.erode(inversion, kernel, iterations=1) 
dilatacion = cv2.dilate(erosion, kernel, iterations=1)

#contamos los objetos encontrados
gauss = cv2.GaussianBlur(dilatacion, (5, 5), 0)
contornos,_ = cv2.findContours(dst,1,1) #cuenta los bordes encontrados
print("-------------------------------------------------------------------------")
areas = [cv2.contourArea(c) for c in contornos]
i = 0
for c in areas:
    if(c > 600):
        i = i + 1

print("He encontrado {} rectangulos".format(i-1))

cv2.imshow("Image_Binario",dst)
cv2.imshow("Dilatacion",dilatacion)
#cv2.imshow("Erosion",erosion)
cv2.waitKey(0)
cv2.destroyAllWindows()