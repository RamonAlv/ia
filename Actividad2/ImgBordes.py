import numpy as np
import cv2

img = 'LibAb.jpg'

src = cv2.imread(img)

gray = cv2.imread(img, cv2.IMREAD_GRAYSCALE)       

'''
THRESH_TRUNC
THRESH_TOZERO
THRESH_TOZERO_INV

'''
gray = cv2.GaussianBlur(gray, (9, 9), 3)

kernel = np.ones((2,2),np.uint8)

# erosion = cv2.erode(gray,kernel,iterations = 1)
dilation = cv2.dilate(gray,kernel,iterations = 1)
# opening = cv2.morphologyEx(gray, cv2.MORPH_OPEN, kernel)
# closing = cv2.morphologyEx(gray, cv2.MORPH_CLOSE, kernel)
# gradient = cv2.morphologyEx(gray, cv2.MORPH_GRADIENT, kernel)
# tophat = cv2.morphologyEx(gray, cv2.MORPH_TOPHAT, kernel)
# blackhat = cv2.morphologyEx(gray, cv2.MORPH_BLACKHAT, kernel)

# UmG = cv2.GaussianBlur(dilation, (7, 7), 3)

t, dst = cv2.threshold(dilation, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)


'''gray = cv2.GaussianBlur(gray, (7, 7), 3)

t, dst = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)

contours = cv2.findContours(dst, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]

for c in contours:
    area = cv2.contourArea(c)
    if area > area/2 and area < (area*2)+1:
        img = cv2.drawContours(src, [c], 0, (0, 255, 0), 2, cv2.LINE_AA)
        
cv2.imwrite("Contorno.jpg", img)

t, img = cv2.threshold(img, 170, 255, cv2.THRESH_BINARY)'''

# cv2.imshow('Original', src)
# cv2.imshow('Grises', gray)
cv2.imshow('Binario', dst)
cv2.imshow('Bordes', dilation)

cv2.waitKey(0)