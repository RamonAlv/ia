import numpy as np
import cv2
from matplotlib import pyplot as plt

img = 'LibAb.jpg'

gray = cv2.imread(img, cv2.IMREAD_GRAYSCALE)

umbralOtsu, imgOtsu = cv2.threshold(gray,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
t, ImgBinario = cv2.threshold(gray,127,255,cv2.THRESH_BINARY)
print(umbralOtsu)
t, dst = cv2.threshold(gray, umbralOtsu, 255, cv2.THRESH_BINARY)
print(t)
# gray = cv2.imread(img)

histograma = cv2.calcHist([gray], [0], None, [256], [0, 256])

plt.plot(histograma, color='gray' )
l = str(umbralOtsu)
plt.title('Imagen original umbral utilizado = ' + l)
plt.xlabel('intensidad de iluminacion')
plt.ylabel('cantidad de pixeles')
plt.axvline(umbralOtsu, color='G', linestyle='dashed', linewidth=1)

cv2.imshow('Grises', gray)
cv2.imshow('result', dst)
cv2.imshow('Binaria', ImgBinario)
plt.show()

cv2.waitKey(0)