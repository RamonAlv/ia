import cv2
import numpy as np
from tkinter.filedialog import askopenfilename
imgen = askopenfilename()

src = cv2.imread(imgen)

gray = cv2.cvtColor(src, cv2.COLOR_BGR2GRAY)

t, dst = cv2.threshold(gray, 200, 250, cv2.THRESH_BINARY)
inver = np.invert(dst)
# kernel = np.ones((4,4),np.uint8)
# kernel = cv2.getStructuringElement(cv2.MORPH_HITMISS, (25,25))
kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (20, 20))
# result = cv2.morphologyEx(inver, cv2.MORPH_HITMISS, kernel)
# mask = cv2.morphologyEx(dst, cv2.MORPH_OPEN, kernel)
# mask = cv2.morphologyEx(result, cv2.MORPH_CLOSE, kernel)
# cv2.imshow('hit', result)
# cv2.imwrite('hit.jpg', result)
# mask = np.invert(mask)

# dilation2 = cv2.dilate(mask,kernel,iterations = 1)
erosion = cv2.erode(inver,kernel,iterations = 1)
dilation = cv2.dilate(erosion,kernel,iterations = 1)
d = dilation
cv2.imshow('dilatado', dilation)
cv2.imwrite('dilatados.jpg', d)
gray = cv2.GaussianBlur(dilation, (5, 5), 0)

# t, dst = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_TRIANGLE)

# d = cv2.cvtColor(inver, cv2.COLOR_BGR2GRAY)

contours = cv2.findContours(d, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
i=0
Objetos = 0
for c in contours:
    perimetro = cv2.arcLength(c ,False)
    approx = cv2.approxPolyDP(c, 0.04 * perimetro, True)
    print(len(approx))
    if len(approx) == 4 and i>0:
        cv2.drawContours(src, [c], 0, (0, 255, 0), 2, cv2.LINE_AA)
        Objetos = Objetos+1
        
    i=i+1
Num = "Cuadrados: " + str(Objetos)
n = str(Objetos)
cv2.imwrite('bordesCuadra'+n+'.jpg', src)
cv2.imshow(Num, src)

# cv2.imshow('umbral', dst)

cv2.waitKey(0)