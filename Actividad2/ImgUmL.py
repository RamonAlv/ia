import numpy as np
import cv2
from tkinter.filedialog import askopenfilename
img = askopenfilename()


gray = cv2.imread(img, cv2.IMREAD_GRAYSCALE)

# gray = cv2.medianBlur(gray, 5)
# # umbral fijo
# _, dst1 = cv2.threshold(gray, 100, 255, cv2.THRESH_BINARY)

# cv2.imshow('umbral fijo', dst1)


gray = cv2.medianBlur(gray, 5)

# dst = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.TH RESH_BINARY, 11, 2)
dst2 = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 2)

dst2 = cv2.medianBlur(dst2, 5)

gray = cv2.imread(img)

cv2.imshow('umbral', gray)
cv2.imshow('result', dst2)
cv2.imwrite("UmbraLocal.jpg", dst2)

cv2.waitKey(0)