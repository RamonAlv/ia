import numpy as np
import cv2
from matplotlib import pyplot as plt
from tkinter.filedialog import askopenfilename


img = askopenfilename()

kernel = np.ones((10,10),np.uint8)
image = cv2.imread(img)

gray = cv2.imread(img, cv2.IMREAD_GRAYSCALE)

dilatacion = cv2.dilate(gray, kernel, iterations=1)
# erosion = cv2.erode(dilatacion,kernel,iterations = 1)
diff_total = cv2.absdiff(dilatacion, gray)#resta para obtener letras
# diff_totalDila = cv2.absdiff(dilatacion, gray)

umbralOtsu, imgOtsu = cv2.threshold(diff_total,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
print(umbralOtsu)
t, dst = cv2.threshold(diff_total, umbralOtsu, 255, cv2.THRESH_BINARY)
suma = dilatacion+dst
mask = np.invert(diff_total)
cv2.imwrite("sumaFondo.jpg", diff_total)
# cv2.imshow("fondo Erocion", erosion)
cv2.imshow("Imagen Original", image)
cv2.imwrite("Imagen Original.jpg", image)
cv2.imshow("fondo Dilatacion", dilatacion)
cv2.imwrite("fondoDilatacion.jpg", dilatacion)
# cv2.imshow("fondo Suma", suma)

# cv2.imshow("Resta", diff_total)
# cv2.imshow("Resta Dilatacion", diff_totalDila)
cv2.imshow("binario G", dst)
cv2.imwrite("binarioG.jpg", dst)
cv2.waitKey(0)
cv2.destroyAllWindows()