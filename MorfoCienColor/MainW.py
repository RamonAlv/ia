from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import cv2 as cv
import numpy as np

class MainW(QWidget):
    
    def __init__(self):
        super().__init__()

        self.leftPanel = QVBoxLayout()
        self.btnLoadImages = QPushButton("Cargar imágene")

        # Integrando al panel
        self.leftPanel.addWidget(self.btnLoadImages)