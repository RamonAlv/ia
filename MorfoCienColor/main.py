import sys
from PyQt5.QtWidgets import QApplication
from MainW import MainW

if __name__ == "__main__":
    app = QApplication(sys.argv)
    w = MainW()
    w.show()
    sys.exit(app.exec_())