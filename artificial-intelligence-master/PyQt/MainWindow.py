from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from Metodos import Metodos

import cv2 as cv
import numpy as np
import time

class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.leftPanel = QVBoxLayout()
        # self.textarea = QPlainTextEdit()
        self.btnLoadImages = QPushButton("Cargar imágenes")
        self.btnSelectROI = QPushButton("Seleccionar ROI")
        self.btnExecute = QPushButton("Ejecutar")
        self.algorithmLabel = QLabel("Algoritmo de búsqueda:")
        self.alphaLabel = QLabel("Alfa:")
        self.algorithmComboBox = QComboBox()
        self.alphaSpinBox = QDoubleSpinBox()
        self.vSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.leftPanel.addWidget(self.btnLoadImages)
        self.leftPanel.addWidget(self.btnSelectROI)
        self.leftPanel.addWidget(self.algorithmLabel)
        self.leftPanel.addWidget(self.algorithmComboBox)
        # self.leftPanel.addWidget(self.textarea)
        self.leftPanel.addWidget(self.alphaLabel)
        self.leftPanel.addWidget(self.alphaSpinBox)
        self.leftPanel.addSpacerItem(self.vSpacer)
        self.leftPanel.addWidget(self.btnExecute)
        self.horizontalLayout = QHBoxLayout()
        self.hSpacer1 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.hSpacer2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.vSpacer1 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.vSpacer2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)
        self.imageLabel = QLabel()
        self.verticalLayout = QVBoxLayout()
        self.verticalLayout.addSpacerItem(self.vSpacer1)
        self.verticalLayout.addWidget(self.imageLabel)
        self.verticalLayout.addSpacerItem(self.vSpacer2)
        self.horizontalLayout.addSpacerItem(self.hSpacer1)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.horizontalLayout.addSpacerItem(self.hSpacer2)
        self.mainLayout = QHBoxLayout()
        self.mainLayout.addLayout(self.leftPanel)
        self.mainLayout.addLayout(self.horizontalLayout)
        self.setLayout(self.mainLayout)
        self.btnSelectROI.setEnabled(False)
        self.btnExecute.setEnabled(False)
        algorithms = ["Sum of Squared Diferences",
                      "Sum of Absolute Diferences",
                      "Cross Correlation"]
        self.algorithmComboBox.addItems(algorithms)
        self.alphaSpinBox.setRange(0.01, 0.9)
        self.alphaSpinBox.setSingleStep(0.1)

        self.Align = Metodos()

        self.btnLoadImages.clicked.connect(self.on_btnLoadImages_clicked)
        self.btnSelectROI.clicked.connect(self.on_btnSelectROI_clicked)
        self.btnExecute.clicked.connect(self.on_btnExecute_clicked)

    def on_btnLoadImages_clicked(self):
        self.files, _ = QFileDialog.getOpenFileNames(
                            self,
                            "Selecciona el conjunto de imágenes",
                            "../../../Python",
                            "Archivos de Imagen (*.png *.jpg *.jpeg *.bmp)")
        if len(self.files) < 2:
            QMessageBox.information(self, "Info", "Debe cargar al menos 3 imágenes.")
        else:
            qImage = QImage(self.files[0])
            pixmap = QPixmap.fromImage(qImage)
            self.imageLabel.setPixmap(pixmap)
            self.btnSelectROI.setEnabled(True)

    def on_btnSelectROI_clicked(self):
        image = cv.imread(self.files[0])
        self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height = cv.selectROI(image, False, False)
        cv.destroyAllWindows()

        if self.ROI_width > 0 and self.ROI_height > 0:
            image = cv.imread(self.files[0], 0)
            self.ROI = image[self.ROI_y:self.ROI_y+self.ROI_height, self.ROI_x:self.ROI_x+self.ROI_width]

            qImage = QImage(self.files[0])
            pixmap = QPixmap.fromImage(qImage)
            painter = QPainter()
            painter.begin(pixmap)
            painter.setPen(Qt.green)
            painter.drawRect(self.ROI_x, self.ROI_y, self.ROI_width, self.ROI_height)
            painter.end()
            self.imageLabel.setPixmap(pixmap)
            self.btnExecute.setEnabled(True)

    def on_btnExecute_clicked(self):

        alpha = self.alphaSpinBox.value()
        beta = 1 - alpha

        self.background = cv.imread(self.files[0])
        
        Opccion = str(self.algorithmComboBox.currentText())
        print(Opccion)

        init_time = time.time()
        for i in range(1, len(self.files)):
            print("imagen", i)
            aligned_image = self.Align.AlignImg(Opccion, self.files, i, self.ROI, self.ROI_height, self.ROI_width, self.ROI_x, self.ROI_y)
            self.background = (beta*self.background) + (alpha*aligned_image)

        elapsed_time = time.time() - init_time
        print("Duración:", elapsed_time, "segundos")
        cv.imwrite("background.jpg", self.background)
        ImB = cv.imread("background.jpg")
        cv.imshow(Opccion, ImB)
