from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

import cv2 as cv
import numpy as np

class Metodos():

    def __init__(self):
        self.saltos = ""
    
    def AlignImg(self, seleccion, file, index, ROI, ROI_height, ROI_width, ROI_x, ROI_y):

        shifted_image = cv.imread(file[index], 0)
        
        if seleccion == "Sum of Squared Diferences":
            min = 99**99

            for y in range(shifted_image.shape[0] - ROI_height):
                for x in range(shifted_image.shape[1] - ROI_width):
                    ssd = 0
                    temp_matrix = shifted_image[y:y+ROI_height, x:x+ROI_width]
                    ssd = np.sum((ROI - temp_matrix) ** 2)
                    if ssd < min:
                        min = ssd
                        pos = [x, y]      
        elif seleccion == "Sum of Absolute Diferences":
            min = 99**99

            for y in range(shifted_image.shape[0] - ROI_height):
                for x in range(shifted_image.shape[1] - ROI_width):
                    ssd = 0
                    temp_matrix = shifted_image[y:y+ROI_height, x:x+ROI_width]
                    ssd = np.sum(abs((ROI - temp_matrix)))
                    if ssd < min:
                        min = ssd
                        pos = [x, y]
        else:
            min = 0

            for y in range(shifted_image.shape[0] - ROI_height):
                for x in range(shifted_image.shape[1] - ROI_width):
                    ssd = 0
                    temp_matrix = shifted_image[y:y+ROI_height, x:x+ROI_width]
                    ssd = np.sum(ROI * temp_matrix)
                    if ssd > min:
                        min = ssd
                        pos = [x, y]

        deltaX = ROI_x - pos[0]
        deltaY = ROI_y - pos[1]

        # self.saltos = self.saltos + "deltaX: " + str(deltaX) + "\n" + "deltaY:" + str(deltaY) + "\n"
        # print(self.saltos)
        print("deltaX:", deltaX)
        print("deltaY:", deltaY)

        shifted_image = cv.imread(file[index])
        transform_matrix = np.float32([[1,0,deltaX],[0,1,deltaY]])
        aligned_image = cv.warpAffine(shifted_image, transform_matrix, (shifted_image.shape[1], shifted_image.shape[0]))

        return aligned_image

        def saltosPo(self, pos):
            pos = self.saltos
            return pos

