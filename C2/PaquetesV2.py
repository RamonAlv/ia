import random
import numpy as np
import operator

print('contenedor de paquetes K')

k = int(input())

print('Peso de paquetes NMAX')

nmax = int(input())

print('Buffer de paquetes M')

m = int(input())

print('Individuos maximos TMAX')

Tmax = int(input())

print('Individuos iniciantes T0')

T0 = int(input())

PesoPaqueteK = dict()

for i in range(0, k):
    PesoPaqueteK[str(i)] = random.randint(1, nmax)

# PesoPaqueteK = sorted(PesoPaqueteK.items(), key=operator.itemgetter(1), reverse=True) 
print('Peso de los paquetes')
print(PesoPaqueteK)
print()

individuos = np.empty(shape = (T0, k), dtype = int)

print('Individuos')
for i in range(0, T0):
    index = random.sample(range(0, k), k)
    for j in range(0, k):
        individuos[i][j] = index[j]

